#!/bin/bash

# Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Tap Homebrew Cask
brew tap homebrew/cask

# Update and upgrade Homebrew
brew update && brew upgrade

# Install Homebrew Formulae (Command-Line Tools and Utilities)
brew install \
    autoenv \
    autojump \
    bat \
    cloc \
    cloud-sql-proxy \
    colima \
    composer \
    docker \
    fastd \
    gcloud \
    git \
    go \
    htop \
    kubectl \
    mackup \
    maven \
    notion \
    nikolaeu/numi/numi-cli \
    node \
    ocrmypdf \
    openai \
    php@8.3 \
    postgresql@14 \
    powerlevel10k \
    prettyping \
    siege \
    speedtest-cli \
    tesseract-lang \
    tldr \
    yarn

# Install Homebrew Cask Applications (macOS GUI Applications)
brew install --cask \
    1password \
    1password-cli \
    Alfred \
    bartender \
    dash \
    discord \
    docker \
    Dropbox \
    flux \
    google-chrome \
    google-cloud-sdk \
    istat-menus \
    keybase \
    keybase \
    maccy \
    miro \
    nordvpn \
    notion \
    numi \
    openai \
    powerlevel10k \
    prettier \
    rectangle \
    signal \
    skype \
    slack \
    spotify \
    Tableau \
    tldr \
    unrar \
    vanilla \
    visual-studio-code \
    vlc \
    whatsapp \
    xmind \
    zoom