# My personal setup

Check install.sh

```
brew cleanup && \
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" && \
git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k && \
brew tap homebrew/cask-fonts && brew cask install font-source-code-pro && \
mkdir -p ~/proj/work && mkdir -p ~/proj/personal

```
- install dropbox
- `cd ~/Dropbox/dotfiles && python3 linkit.py`
- `mackup -f restore`
- Download from appstore
    - Monosnap
    - Things

## Setup Tutorial
Watch the installation walkthrough video:

[![First settings in Mac](https://img.youtube.com/vi/Kft9Y33oc2I/0.jpg)](https://www.youtube.com/watch?v=Kft9Y33oc2I)


https://www.beeper.com/download