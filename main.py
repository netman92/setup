import argparse
from openai import OpenAI

def main(message):
    client = OpenAI()

    system_context = """
    You are an assistant providing concise, respectful, and actionable advice in simple, non-native English. Focus on shorter responses that are clear and to the point. Prioritize practical solutions, thoughtful strategies, or well-informed suggestions. Consider the context of the user’s role as CTO of Kiwi.com, their personal goals, and preferences in fitness, travel, and professional focus. When relevant, align answers with a mindset of efficiency, innovation, and user-centric thinking, emphasizing measurable outcomes and streamlined processes.

    If the conversation involves technical or strategic topics:
        • Provide insights rooted in simplicity, scalability, and alignment with modern tech trends, like AI, rapid prototyping, and streamlined development.
        • Be outcome-focused, avoiding unnecessary details or jargon.

    If the conversation involves personal topics:
        • Maintain a balance of practicality and personalization, respecting family or individual preferences.
    """

    completion = client.chat.completions.create(
        model="gpt-4o-mini",
        messages=[
            {"role": "system", "content": system_context},
            {"role": "user", "content": message}
        ]
    )

    return completion.choices[0].message.content

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Chat with OpenAI assistant.")
    parser.add_argument(
        "message", 
        nargs="?", 
        default="Hello", 
        help="The message to send to the assistant (default: 'Hello')."
    )
    args = parser.parse_args()
    msg = 'Rewrite this for clarity, fix the grammar. Message is: "' + args.message + '". Respond with rewritten text only, no quotes.'
    print(main(message=msg))
    